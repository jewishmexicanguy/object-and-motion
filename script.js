class verticie
{
  /**
   * Construct this object, if no dy and dx is given they are set to 0
   * @param {Decimal} x, position in x plane 
   * @param {Decimal} y, position in y plane 
   * @param {Decimal} dx, change in x position to be applied in each animation cycle 
   * @param {Decimal} dy, change in y position to be applied in each animation cycle
   * @param {hex} color, the color to be applied to this vertex, by default it is 'ffffff'
   */
  constructor(x, y, dy = 0, dx = 0, color = '#ffffff')
  {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.color = color;
  }
  /**
   * Update x and y positions according to dx and dy respectivley
   * @param {canvas} canvas, the canvas where the vertex is being animated
   */
  update(canvas)
  {
    this.x = (this.x + this.dx);
    this.y = (this.y + this.dy);
    // we now need to catch for when we update off the canvas and place the vertext on the canvas
    if (this.x > canvas.width)
    {
      this.x = 0;
    }
    if (this.y > canvas.height)
    {
      this.y = 0;
    }
    if (this.x < 0)
    {
      this.x = canvas.width;
    }
    if (this.y < 0)
    {
      this.y = canvas.height;
    }
  }
  /**
   * Set the change in x
   * @param {Decimal} dx
   */
  setDX(dx){this.dx = dx;}
  /**
   * Set the change in y
   * @param {Decimal} dy 
   */
  setDY(dy){this.dy = dy;}
  /**
   * Set the color of this Vertex
   * @param {hex} color 
   */
  setColor(color){this.color = color;}
}

class Shape 
{
  /**
   * Constructor of this object
   * @param {Array} vertices, is an array of objects, where each object is a verticie 
   */
  constructor(vertices) 
  {
    this.vertices = vertices;
  }
  /**
   * This method is meant to be overriden by derived types
   * The idea is to get back an array of verticies that can be used to instantiate an animation of a derived class
   * @param {Array} args 
   */
  static getScafold(args)
  {
    throw "Not Implemented Exception";
  }
  /**
   * The idea is that the current velocities of this shap will be applied to each verticie on each animation cycle
   * @param {canvas} canvas, the canvas where the shape is being drawn
   */
  update(canvas)
  {
    this.vertices.forEach((i)=>{i.update(canvas);});
  }
  /**
   * This method needs to be overriden by derived types
   * @param {canvas.context} ctx, canvas context
   */
  draw(ctx)
  {
    throw "Not Implemented Exception";
  }
}

class Rectanlge extends Shape
{
  /**
   * Check to make sure 4 vertecies are supplied
   * @param {Array} vertices 
   */
  constructor(vertices)
  {
    if ((vertices.length) == 4)
    {
      super(vertices);
    }
    else
    {
      console.log(vertices.length);
      throw "Rectanlges have 4 vertexes";
    }
  }
  /**
   * The idea is to accept an initial point and a step to build more verticies, returning an array of verticies that will trace the outside of a rectangle.
   * @param {object} args 
   *    Verticie : the begining vertex to scaffold from
   *    Step : integer or decimal that tells us how far away in x and y the next verticies should be from each other 
   */
  static getScafold(args)
  {
    let vertex = args.vertex;
    let step = args.step;
    // rectangles have 4 verticies
    let b = new verticie(
      (vertex.x + step), 
      vertex.y,
      vertex.dy,
      vertex.dx,
      vertex.color
    );
    let c = new verticie(
      (vertex.x + step),
      (vertex.y + step),
      vertex.dy,
      vertex.dx,
      vertex.color
    );
    let d = new verticie(
      vertex.x,
      (vertex.y + step),
      vertex.dy,
      vertex.dx,
      vertex.color
    );
    return [vertex, b, c, d];
  }
  draw(ctx)
  {
    // draw the point and the lines
    for (let i = 0; i < this.vertices.length; i++)
    {
      ctx.beginPath();
      ctx.arc(
        this.vertices[i].x, 
        this.vertices[i].y, 
        1, 
        0, 
        (2 * Math.PI), 
        false
      );
      ctx.fillStyle = this.vertices[i].color;
      ctx.fill();
      ctx.moveTo(this.vertices[i].x, this.vertices[i].y);
      // catch when this is at last vertex, draw line back to beggining vertex
      if (i == (this.vertices.length - 1)) 
      {
        ctx.lineTo(this.vertices[0].x, this.vertices[0].y);
      }
      else 
      {
        ctx.lineTo(this.vertices[i + 1].x, this.vertices[i + 1].y);
      }
      ctx.strokeStyle = this.vertices[i].color;
      ctx.stroke();
    }
  }
}

var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

var vertex1 = new verticie(200, 160, -1.5, -1.5, '#c3e4ff');
var vertex2 = new verticie(320, 120, 2.5, 2.5, '#eec3ff');
var vertex3 = new verticie(220, 320, 4, -4, '#fff7c3');
var vertex4 = new verticie(400, 420, -1, 1, '#ffd8c3');
//console.log(vertex1);
var arr1 = Rectanlge.getScafold({'vertex' : vertex1, 'step' : 25});
var rec1 = new Rectanlge(arr1);
var rec2 = new Rectanlge(Rectanlge.getScafold({'vertex': vertex2, 'step' : 50}));
var rec3 = new Rectanlge(Rectanlge.getScafold({'vertex': vertex3, 'step' : 37.50}));
var rec4 = new Rectanlge(Rectanlge.getScafold({'vertex': vertex4, 'step' : 37.50}));
var rectangles = [];
rectangles.push(rec1);
rectangles.push(rec2);
rectangles.push(rec3);
rectangles.push(rec4);
// animation loop
function loop()
{
  ctx.clearRect(0, 0, canvas.clientWidth, canvas.height);
  for (let i = 0; i < rectangles.length; i++)
  {
    rectangles[i].update(canvas);
    rectangles[i].draw(ctx);
  }
  requestAnimationFrame(loop);
}

// start animation loop
loop();